variable "ec2_image" {
  default = "ami-053b0d53c279acc90"
}

variable "ec2_instance_type" {
  default = "t2.micro"
}

#variable "ec2_keypair" {
 # default = "AWS-123"
#}

variable "ec2_tags" {
  default = "Test-Terraform"
}

variable "ec2_count" {
  default = "1"
}
